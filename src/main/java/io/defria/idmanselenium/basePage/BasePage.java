package io.defria.idmanselenium.basePage;

import io.defria.idmanselenium.pages.homePage.HomePage;
import io.defria.idmanselenium.util.Helper;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {

    protected WebDriver driver;
    protected Logger logger;

    public BasePage(WebDriver driver, Logger logger) {
        this.driver = driver;
        this.logger = logger;
    }

    public String getCurrentPageUrl() {
        return driver.getCurrentUrl();
    }

    protected void click(By locator) {
        waitForVisibilityOf(locator, getTimeout());
        find(locator).click();
    }

    protected HomePage getHomePage() {
        return new HomePage(driver, logger);
    }

    protected void openUrl(String url) {
        driver.get(url);
    }

    private void waitFor(ExpectedCondition<WebElement> condition, Integer timeOutInSeconds) {
        timeOutInSeconds = timeOutInSeconds != null ? timeOutInSeconds : getTimeout();
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.until(condition);
    }

    private int getTimeout() {
        return 10;
    }

    public WebElement find(By locator) {
        return driver.findElement(locator);
    }

    protected void type(String value, By field) {
        if (!Helper.isStringNullOrWhitespace(value)) {
            waitForVisibilityOf(field, getTimeout());
            find(field).sendKeys(value);
        }
    }

    protected void waitForVisibilityOf(By locator, Integer... timeOutInSeconds) {
        int attempts = 0;
        while (attempts < 2) {
            try {
                waitFor(ExpectedConditions.visibilityOfElementLocated(locator),
                        (timeOutInSeconds.length > 0 ? timeOutInSeconds[0] : null));
                break;
            } catch (StaleElementReferenceException e) {
                e.printStackTrace();
            }
            attempts++;
        }
    }
}
