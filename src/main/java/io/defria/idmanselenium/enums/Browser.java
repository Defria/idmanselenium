package io.defria.idmanselenium.enums;

public enum Browser {CHROME, FIREFOX, INTERNET_EXPLORER}