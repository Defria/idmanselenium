package io.defria.idmanselenium.util;

public class Helper {

    public static boolean isStringNullOrWhitespace(String value) {
        // checks for at least one (ASCII) alphanumeric character
        return value == null || !value.matches(".*\\w.*");
    }

    public static void threadSleep(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
