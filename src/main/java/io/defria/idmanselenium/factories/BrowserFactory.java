package io.defria.idmanselenium.factories;

import io.defria.idmanselenium.enums.Browser;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;

public class BrowserFactory {
    private final ThreadLocal<WebDriver> driver = new ThreadLocal<>();
    private final Browser browser;
    private final Logger logger;

    public BrowserFactory(Browser browser, Logger logger) {
        this.browser = browser;
        this.logger = logger;
    }

    public WebDriver createDriver() {
        logger.info(String.format("Creating %s browser driver", browser));
        switch (browser) {

            case CHROME:
                createChromeDriver();
                break;

            case FIREFOX:
                createFirefoxDriver();
                break;

            case INTERNET_EXPLORER:
                createIEDriver();
                break;

            default:
                logger.info("Unregistered browser name: " + browser + ", starting default browser CHROME instead");
                createChromeDriver();
                break;
        }
        return driver.get();
    }

    private void createIEDriver() {
        WebDriverManager.iedriver().setup();
        InternetExplorerOptions options = new InternetExplorerOptions();
        options.ignoreZoomSettings();
        options.disableNativeEvents();
        options.introduceFlakinessByIgnoringSecurityDomains();
        driver.set(new InternetExplorerDriver(options));
    }

    private void createChromeDriver() {
        WebDriverManager.chromedriver().setup();
        driver.set(new ChromeDriver());
    }

    private void createFirefoxDriver() {
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
        WebDriverManager.firefoxdriver().setup();
        driver.set(new FirefoxDriver(firefoxOptions));
    }
}
