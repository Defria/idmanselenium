package io.defria.idmanselenium.pages.homePage;

import org.openqa.selenium.By;

public class HomePageElement {
    public static By userNameField = By.id("loginUsername");
    public static By passwordField = By.id("loginPassword");
    public static By loginButton = By.id("login");
}
