package io.defria.idmanselenium.pages.homePage;

import io.defria.idmanselenium.basePage.BasePage;
import io.defria.idmanselenium.configuration.Configuration;
import io.defria.idmanselenium.pages.identitiesPage.IdentitiesWebElement;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class HomePage extends BasePage {


    public HomePage(WebDriver driver, Logger logger) {
        super(driver, logger);
    }

    public void clickLogin(String username, String password) {
        openUrl(Configuration.baseUrl);
        type(username, HomePageElement.userNameField);
        type(password, HomePageElement.passwordField);
        click(HomePageElement.loginButton);
        waitForVisibilityOf(IdentitiesWebElement.validateButton);
    }
}
