package io.defria.idmanselenium.pages.identitiesPage;

import io.defria.idmanselenium.basePage.BasePage;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class IdentitiesPage extends BasePage {

    public IdentitiesPage(WebDriver driver, Logger logger) {
        super(driver, logger);
    }

    public void validate(String idNumber) {
        getHomePage().clickLogin("john", "password");
        type(idNumber, IdentitiesWebElement.idNumberField);
        click(IdentitiesWebElement.validateButton);
    }
}
