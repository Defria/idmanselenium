package io.defria.idmanselenium.pages.identitiesPage;

import org.openqa.selenium.By;

public class IdentitiesWebElement {
    public static By validateButton = By.id("validate");
    public static By idNumberField = By.id("idNumber");
}
