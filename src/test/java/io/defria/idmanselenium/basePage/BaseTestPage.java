package io.defria.idmanselenium.basePage;

import io.defria.idmanselenium.enums.Browser;
import io.defria.idmanselenium.factories.BrowserFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import java.lang.reflect.Method;

public abstract class BaseTestPage {
    protected WebDriver driver;
    protected Logger logger;

    @Parameters({"browser"})
    @BeforeMethod(alwaysRun = true)
    public void setUp(Method method, @Optional("chrome") String browser, ITestContext context) {
        logger = LogManager.getLogger(context.getCurrentXmlTest().getName());
        initializeWebDriver(browser);
    }

    private void initializeWebDriver(@Optional("chrome") String browser) {
        Browser browserName = Browser.valueOf(browser.toUpperCase());
        BrowserFactory factory = new BrowserFactory(browserName, logger);
        driver = factory.createDriver();
        driver.manage().window().maximize();
    }

    public String getBrowserName() {
        return ((RemoteWebDriver) driver).getCapabilities().getBrowserName().toUpperCase();
    }

    @AfterMethod()
    public void tearDown(ITestResult result) {
        int status = result.getStatus();
        logger.info("Test result exit code " + status);
        int success = 1;
        if (status != success) {
            logger.info("Take screenshot");
        }
        logger.info(String.format("Close browser %s", getBrowserName()));
        if (driver != null)
            driver.quit();
    }
}
