package io.defria.idmanselenium.identitiesPage;

import io.defria.idmanselenium.basePage.BaseTestPage;
import io.defria.idmanselenium.pages.identitiesPage.IdentitiesPage;
import io.defria.idmanselenium.pages.identitiesPage.IdentitiesWebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class IdentitiesPageTests extends BaseTestPage {
    @Test
    public void testValidate() {
        // Arrange
        IdentitiesPage identitiesPage = new IdentitiesPage(driver, logger);

        // Act
        identitiesPage.validate("8801234800186");

        // Assert
        Assert.assertTrue(identitiesPage.find(IdentitiesWebElement.validateButton).isEnabled());
    }
}
