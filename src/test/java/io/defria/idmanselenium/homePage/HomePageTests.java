package io.defria.idmanselenium.homePage;

import io.defria.idmanselenium.basePage.BaseTestPage;
import io.defria.idmanselenium.pages.homePage.HomePage;
import org.testng.Assert;
import org.testng.annotations.Test;


public class HomePageTests extends BaseTestPage {

    @Test
    public void testLogin() {
        // Arrange
        HomePage homePage = new HomePage(driver, logger);

        // Act
        homePage.clickLogin("john", "password");

        // Assert
        Assert.assertEquals(homePage.getCurrentPageUrl(), "http://localhost:4200/identities");
    }
}
