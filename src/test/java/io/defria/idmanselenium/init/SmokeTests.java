package io.defria.idmanselenium.init;

import io.defria.idmanselenium.basePage.BaseTestPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SmokeTests extends BaseTestPage {
    @Test(priority = 1, description = "Test to check if we can start a browser")
    public void testStartingBrowser() {
        // Arrange
        String baseUrl = "http://localhost:4200/";

        // Act
        driver.get(baseUrl);

        // Assert
        Assert.assertEquals(driver.getCurrentUrl(), baseUrl);
    }
}
